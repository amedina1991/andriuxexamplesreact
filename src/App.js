import React from 'react';
import logo from './logo.svg';
import './App.css';
import  Header  from './components/Header.js';
import Formulario from './components/Formulario.js';
import Blog from './components/Blog.js';
import BlogDinamic  from './components/BlogDinamic.js';
import PruebaHk from './components/PruebaHk.js';


function App() {
  return (
    <div className="App">
        <Header />
        <Formulario />
        <Blog />
        <BlogDinamic />
        <PruebaHk />
    </div>
  );
}

export default App;
