import React from 'react';

export default class BlogDinamic extends React.Component{

	constructor(props){
		super(props);

		this.state = {
			articles:[]
		}
	}

	componentDidMount(){
		let promesa = fetch('http://localhost:5000/getData');
		promesa.then( (response) => response.json()).then(data =>{
			this.setState({
			    articles: data
			})
		})
	}

	render(){
		return (
			<div>
				{	
					this.state.articles.map((result) =>{
						return <h2>{result.title}</h2>
					})
				}
			</div>
		)
	}
}