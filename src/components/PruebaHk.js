
import React ,{useState} from 'react';

function Contador2(props){
	const [contador,setContador] = useState(1);
	return (
 		<div>
 			<p>{contador}</p>
 			<button onClick={()=>{setContador(contador+1)}}>Asi es</button>
 		</div>	
	)
}

export default class PruebaHk extends React.Component{
	constructor(props){
		super(props);
	}
	render(){
		return (
		<Contador2 />
		)
	}

}
