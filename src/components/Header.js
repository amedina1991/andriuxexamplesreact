import React from 'react';

export default class Header extends React.Component{

	constructor(props){
		super(props);
		this.state = {
			contador2: 0
		}
	}

	aumentar = () => {
		this.setState({contador2: this.state.contador2 + 1 })
	};

	render(){
		return(
			<div>
				<p>{this.state.contador2}</p>
				<button onClick={ this.aumentar }>Aumentar</button>
			</div>
			);
	}
}