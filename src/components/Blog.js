import React ,{Fragment}from 'react';

export default class Blog extends React.Component{

	constructor(props){
		super(props);

		this.state = {
			articles:[
				{ 
					title:'Muertos el dia de hoy en la alameda',
					description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi corporis porro, quam! Quidem ut voluptate iste, voluptatum cumque impedit ipsum odit aspernatur unde eos quia rem vero a, blanditiis officia.'	
		    	},
		    	{ 
					title:'Matasuegra',
					description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi corporis porro, quam! Quidem ut voluptate iste, voluptatum cumque impedit ipsum odit aspernatur unde eos quia rem vero a, blanditiis officia.'	
		    	},
		    	{ 
					title:'Superman en casa',
					description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi corporis porro, quam! Quidem ut voluptate iste, voluptatum cumque impedit ipsum odit aspernatur unde eos quia rem vero a, blanditiis officia.'	
		    	}
			]
		};
	}

	render(){
		return (
			<div>
				{
					this.state.articles.map((result)=>{
						return <p>{ result.description }</p>
					})
				}
			</div>
		)
	}
}
	

