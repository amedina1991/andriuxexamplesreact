import React ,{Fragment} from 'react';

export default class Formulario extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			email:'',
			password:''
		}
	}

	changeGetValuesForm(value,property){
		let state = {};
		state[property] = value;
		this.setState(state);
	}

	submitForm(){
		console.log(this.state);
	}

	render(){
		return(
			<Fragment>
				<form>
					<input type="email" name="email" value={this.state.email} onChange={(ev)=>{
						this.changeGetValuesForm(ev.target.value,'email');
					}} />
					<input type="password"  name="password" value={this.state.password} onChange={(ev)=>{
						this.changeGetValuesForm(ev.target.value, 'password');
					}}/>
				</form>
				<button type="button" onClick={ () => { this.submitForm() }}>enviar</button>
			</Fragment> 
		)
	}
}